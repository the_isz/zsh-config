# vim: set noet:
autoload -U compinit promptinit colors
compinit
promptinit
colors

# Allow # to be comments in interactive mode, too.
setopt interactivecomments
# Skip duplicate lines while browsing history
setopt histignoredups

export USER_COLOR="green"

if [[ $USERNAME == "root" ]]; then
	export USER_COLOR="red"
fi

which git &> /dev/null

if [[ 0 -eq $? ]]; then
	function git_branch()
	{
		if [ -d .git ]; then
			git symbolic-ref -q HEAD | sed 's%^.*/\([^/].*\)$%\1%;s/^/(/;s/$/) /';
		fi
	};

	setopt PROMPT_SUBST
	PROMPT="%{$fg[$USER_COLOR]%}%n%{$reset_color%}@%{$fg[yellow]%}%m %{$fg[blue]%}"'$(git_branch)'"%{$reset_color%}%{$reset_color%}%1~ "
else
	PROMPT="%{$fg[$USER_COLOR]%}%n%{$reset_color%}@%{$fg[yellow]%}%m %{$reset_color%}%1~ "
fi

alias ..='cd ..'
alias ...='cd ../..'
alias ls='ls -h --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias rsync='rsync --progress'
alias hibernate='dbus-send --system --print-reply --dest="org.freedesktop.UPower" /org/freedesktop/UPower org.freedesktop.UPower.Suspend'
alias xerrs='grep "\(^(WW)\)\|\(^(EE)\)" /var/log/Xorg.0.log'

alias expl-packages="comm -23 <(pacman -Qetq) <(pacman -Qgq base base-devel | sort)"

function findnewconfigs()
{
	if [[ $USERNAME != "root" ]]; then
		echo "Warning: Unless you're root, you won't be able to scan through all directories!"
	fi
	find /etc -regextype posix-extended -regex '.*\.(pacsave|pacnew)$'
};

# remind me, its important!
# usage: remindme <time> <text>
# e.g.: remindme 10m "omg, the pizza"
function remindme()
{
	(sleep $1 && zenity --info --text "$2") &;
}

function findcpp()
{
	find $1 -type f -regextype posix-extended -iregex '.*\.[ch](pp|xx)?$' ! -iregex '.*/(ui|moc)_[^/]*$';
}

function findcppandpro()
{
	find $1 -type f -regextype posix-extended -iregex '.*\.(h|cpp|pro)$' ! -iregex '.*/(ui|moc)_[^/]*$';
}

function fetch_mails_now()
{
	# See this for details
	# https://github.com/brong/brong-offlineimap/commit/e1fb9492f84538df698d6a2f1cfa2738929ed040
	OFFLINEIMAP_PID=$(pidof -x offlineimap)
	if test 0 -eq $?; then
		echo "Fetching now..."
		kill -s USR1 $OFFLINEIMAP_PID
	else
		echo "offlineimap seems not to be running..."
	fi
}

function id3_info()
{
	python - $* <<-EOF
		import sys
		import eyed3.id3

		tag = eyed3.id3.Tag()

		for file in sys.argv[1:]:
		    with open(file, "rb") as mp3_file:
		        tag.parse(mp3_file)

		        none_or = lambda x, l: None if x is None else l(x)
		        quotes  = lambda s: "\"{}\"".format(s)

		        result = \
		            [   ("Artist:       ", quotes(tag.artist))
		            ,   ("Album artist: ", none_or(tag.album_artist, quotes))
		            ,   ("Album:        ", quotes(tag.album))
		            ,   ("Genre:        ", none_or(tag.genre, lambda genre: "{} ({})".format(genre.name, genre.id)))
		            ,   ("Date:         ", tag.release_date)
		            ,   ("Num:          ", tag.track_num)
		            ,   ("Title:        ", quotes(tag.title))
		            ,   ("Disc:         ", tag.disc_num)
		            ,   ("Version:      ", tag.version)
		            ]

			print("\n".join(["".join(map(str, items)) for items in result]))
	EOF
}

export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export DIFFPROG='nvim -d'

chpwd() {
	[[ -t 1 ]] || return
	case $TERM in
		screen|(u)rxvt*) print -Pn "\e]2;%3~\a"
			;;
	esac
}

set -o vi

if [[ $TERM == "screen" && $USERNAME != "root" ]]; then
	which gcalcli &> /dev/null
	if [[ 0 -eq $? ]]; then
		gcalcli calw 2 &
	fi
fi

# vim: set noet:
