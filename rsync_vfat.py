#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import os
import os.path
import shutil
import re
import subprocess


(L_TRACE, L_DEBUG) = (2, 1)
(O_CREATE_DIR, O_SET_TIME, O_COPY, O_DELETE) = range(4)


def createOperationsList(source, dest, delete, delete_excluded, filter_):
    """
    Call rsync with the "--dry-run" flag to create a list of operations to be
    performed.

    @param source:
        Source directory for rsync.

    @param dest:
        Destination directory for rsync.

    @param delete:
        If True, add the "--delete" flag to the rsync call.

    @param delete_excluded:
        If True, add the "--delete-excluded" flag to the rsync call.

    @param filter_:
        If not None, a filter rule to pass to rsync via its "--filter" option.
    """
    rsync_args = ["rsync", "-r", "--times"]

    if delete:
        rsync_args.append("--delete")

    if delete_excluded:
        rsync_args.append("--delete-excluded")

    if filter_ is not None:
        rsync_args.append("--filter={}".format(filter_))

    rsync_args += \
        [ "--info=flist0"     # Disable "sending incremental file list" message
        , "--itemize-changes"
        , "--dry-run"
        , "--modify-window=1" # VFAT has 2-second precision
        , source
        , dest
        ]

    operations = []

    rsync_ops = \
        { "cd+++++++++ ": O_CREATE_DIR
        , ".d..t...... ": O_SET_TIME
        , ">f+++++++++ ": O_COPY # New file
        , ">f.st...... ": O_COPY # (s)ize and (t)ime changed
        , ">f..t...... ": O_SET_TIME
        , "*deleting   ": O_DELETE
        }

    p = subprocess.Popen(rsync_args, stdout=subprocess.PIPE, universal_newlines=True)

    outs, _ = p.communicate()

    for line in outs.splitlines():
        try:
            operations.append((rsync_ops[line[:12]], line[12:]))

        except KeyError:
            raise RuntimeError("Unhandled rsync info output:\n\t'{}'".format(line))

    return operations


INVALID_TRAILING_CHARS = re.compile(r"[. ]*$")
INVALID_CHARS = re.compile(r'[/?<>\:*|"^]+')


def toDosName(path, renames=None):
    result_path = ""
    temp_path   = path

    def replace_invalid_chars(component):
        result = INVALID_TRAILING_CHARS.sub("", component)
        return INVALID_CHARS.sub("_", result)

    def join_unless_empty(c1, c2):
        if "" == c2:
            return c1

        return os.path.join(c1, c2)

    while True:
        head, tail = os.path.split(temp_path)

        if head == temp_path:
            # end of absolute path
            result_path = join_unless_empty(head, result_path)
            break

        if tail == temp_path:
            # end of relative path
            result_path = join_unless_empty(replace_invalid_chars(tail), result_path)
            break

        result_path = join_unless_empty(replace_invalid_chars(tail), result_path)
        temp_path   = head

    if None is not renames:
        if path != result_path:
            renames[result_path] = path

    return result_path


def main():
    parser = argparse.ArgumentParser(description="rsync onto a vfat filesystem")
    parser.add_argument \
        ( "source"
        , metavar="<source>"
        , help="Source file or directory"
        )
    parser.add_argument \
        ( "dest"
        , metavar="<dest>"
        , help="Destination file or directory"
        )
    parser.add_argument \
        ( "-n", "--dry-run"
        , dest="dry_run"
        , help="Perform a trial run with no changes made"
        , action="store_true"
        , default=False
        )
    parser.add_argument \
        ( "--delete"
        , dest="delete"
        , help="Delete extraneous files from dest dirs"
        , action="store_true"
        , default=False
        )
    parser.add_argument \
        ( "--delete-excluded"
        , dest="delete_excluded"
        , help="Also delete excluded files from dest dirs"
        , action="store_true"
        , default=False
        )
    parser.add_argument \
        ( "-v", "--verbose"
        , dest="verbose"
        , help="Increase verbosity. 1: Trace changes, 2: Trace skipped files"
        , action="count"
        , default=0
        )
    parser.add_argument \
        ( "--modify-window"
        , dest="modify_window"
        , help="Compare mod-times with reduced accuracy (default: 1.0)"
        , type=float
        , default=1.0
        )
    parser.add_argument \
        ( "-f", "--filter"
        , metavar="<RULE>"
        , help="Add a file-filtering RULE"
        )

    args = parser.parse_args()

    def vprint(level, fmt, *fmt_args):
        if level > args.verbose:
            return

        print(fmt.format(*fmt_args))

    def has_file_changed(f, reference):
        mtime_diff = \
            abs(os.path.getmtime(reference) - os.path.getmtime(f))

        if mtime_diff > args.modify_window or \
            os.path.getsize(reference) != os.path.getsize(f):
            return True

        vprint(L_TRACE, "skip same '{}'", reference)
        return False

    # Maps renamed files to their original name
    renames = {}

    for op, fname in sorted \
        ( createOperationsList \
            ( args.source
            , args.dest
            , args.delete
            , args.delete_excluded
            , args.filter
            )
        , key=lambda x: x[0]
        ):
        fname = fname.rstrip("/")

        if O_CREATE_DIR == op:
            d = os.path.join(args.dest, toDosName(fname, renames))

            if os.path.isdir(d):
                continue

            if os.path.exists(d):
                vprint(L_DEBUG, "rm        '{}'", d)
                if not args.dry_run:
                    os.remove(d)

            vprint(L_DEBUG, "mkdir     '{}'", d)
            if not args.dry_run:
                os.mkdir(d)

        elif O_SET_TIME == op:
            source = os.path.join(args.source, fname)
            dest   = os.path.join(args.dest  , fname)

            vprint(L_DEBUG, "set time  '{}'", dest)
            if not args.dry_run:
                s_stats = os.stat(source)
                d_stats = os.stat(dest)
                os.utime(dest, (d_stats.st_atime, s_stats.st_mtime))

        elif O_COPY == op:
            source = os.path.join(args.source, fname)

            if os.path.islink(source):
                vprint(L_TRACE, "skip link '{}'", source)
                continue

            dest = os.path.join(args.dest, toDosName(fname, renames))

            if os.path.exists(dest):
                if os.path.isdir(dest):
                    vprint(L_DEBUG, "rmdir     '{}'", dest)
                    if not args.dry_run:
                        shutil.rmtree(dest)

                elif not has_file_changed(dest, source):
                    continue

            vprint(L_DEBUG, "copy      '{}' -> '{}'", source, dest)
            if not args.dry_run:
                shutil.copy2(source, dest)

        elif O_DELETE == op:
            p_source = os.path.join(args.source, renames.get(fname, fname))
            p_dest   = os.path.join(args.dest  , fname)

            if not os.path.exists(p_source):
                if os.path.isdir(p_dest):
                    vprint(L_DEBUG, "rmdir     '{}'", p_dest)
                    if not args.dry_run:
                        shutil.rmtree(p_dest)

                else:
                    vprint(L_DEBUG, "rm        '{}'", p_dest)
                    if not args.dry_run:
                        os.remove(p_dest)

    return 0


if __name__ == "__main__":
    sys.exit(main())
